package com.npti.digio.control.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.npti.digio.R;
import com.npti.digio.control.asynctask.ImageAt;

public class DigioAdapter extends PagerAdapter {

    private Context context;

    private LayoutInflater inflater;

    private String[] list = new String[20];

    public DigioAdapter(Context context, String[] list) {
        this.context = context;
        this.list = list;

        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.layout_test, container, false);
        TextView tv = (TextView) view.findViewById(R.id.tv);
        tv.setText(position+"");
        container.addView(view);

        ImageView iv = view.findViewById(R.id.ivImage);

        ImageAt ia = new ImageAt(iv);
        ia.execute();

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}