package com.npti.digio.view;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.npti.digio.R;
import com.npti.digio.control.adapter.ProductAdapter;
import com.npti.digio.control.adapter.SpotlightAdapter;
import com.npti.digio.control.asynctask.DigioAt;
import com.npti.digio.control.transformer.Transformer;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        this.create(v);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        this.start();
    }

    /**
     *
     */

    /**
     *
     */

    private void create(View v) {
        try {
            ViewPager viewPager = v.findViewById(R.id.vpSpotlight);
            ViewPager viewPager1 = v.findViewById(R.id.vpProduct);

            DigioAt da = new DigioAt(getContext());
            da.execute();

            if (da.get()) {
                SpotlightAdapter adapter = new SpotlightAdapter(getContext(), da.getDto().getSpotlight());
                viewPager.setAdapter(adapter);
                viewPager.setOffscreenPageLimit(4);
                viewPager.setPageMargin((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()));
                viewPager.setPageTransformer(false, new Transformer(getContext()));

                ProductAdapter adapter1 = new ProductAdapter(getContext(), da.getDto().getProducts());
                viewPager1.setAdapter(adapter1);
                viewPager1.setOffscreenPageLimit(4);
                viewPager1.setPageMargin((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()));
                viewPager1.setPageTransformer(false, new Transformer(getContext()));
            }
        } catch (Exception e) {

        }
    }

    private void start() {

    }

}