package com.npti.digio.control.adapter;

import static android.graphics.Color.WHITE;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.npti.digio.R;
import com.npti.digio.control.asynctask.ImageAt;
import com.npti.digio.model.entity.ProductEntity;
import com.npti.digio.model.entity.SpotlightEntity;

import java.util.List;

public class ProductAdapter extends PagerAdapter {

    private Context context;

    private LayoutInflater inflater;

    private List<ProductEntity> list;

    public ProductAdapter(Context context, List<ProductEntity> list) {
        this.context = context;
        this.list = list;

        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.layout_product, container, false);
        view.setBackgroundColor(context.getColor(R.color.white));

        ImageView iv = view.findViewById(R.id.ivImage);
        iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        ImageAt ia = new ImageAt(iv);
        ia.execute(this.list.get(position).getImageURL());

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public float getPageWidth(int position) {
        return(0.5f);
    }

}