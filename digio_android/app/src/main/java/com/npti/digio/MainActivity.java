package com.npti.digio;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import com.npti.digio.view.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Fragment frag = new MainFragment();

        super.getSupportFragmentManager().beginTransaction().add(R.id.fl_content, frag).commit();
    }
}