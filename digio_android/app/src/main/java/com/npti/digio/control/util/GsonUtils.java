package com.npti.digio.control.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.ClassUtils;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class GsonUtils extends ClassUtils {

    public GsonUtils() {
    }

    public static synchronized Gson newInstance() {
        GsonBuilder builder = (new GsonBuilder()).registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong());
            }
        }).registerTypeAdapter(byte[].class, new JsonDeserializer<byte[]>() {
            public byte[] deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
                try {
                    List list = new ArrayList();
                    JsonArray ja = json.getAsJsonArray();
                    Iterator var6 = ja.iterator();

                    while (var6.hasNext()) {
                        JsonElement je = (JsonElement) var6.next();
                        list.add(je.getAsByte());
                    }

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    ObjectOutputStream oos = new ObjectOutputStream(bos);
                    oos.writeObject(list);
                    byte[] bytes = bos.toByteArray();
                    return bytes;
                } catch (Exception var13) {
                    var13.printStackTrace();
                    return null;
                } finally {
                    ;
                }
            }
        }).registerTypeAdapter(Object.class, new JsonDeserializer<Object>() {
            public List<Object> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) {
                List<Object> list = new ArrayList();
                if (json.isJsonArray()) {
                    Iterator var5 = json.getAsJsonArray().iterator();

                    while (var5.hasNext()) {
                        JsonElement e = (JsonElement) var5.next();
                        list.add(ctx.deserialize(e, Object.class));
                    }
                } else {
                    if (!json.isJsonObject()) {
                        throw new RuntimeException("Unexpected JSON type: " + json.getClass());
                    }

                    list.add(ctx.deserialize(json, Object.class));
                }

                return list;
            }
        }).registerTypeAdapter(byte[].class, new JsonSerializer<byte[]>() {
            public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(Base64.encodeBase64String(src));
            }
        });
        return builder.create();
    }

    public static synchronized GsonBuilder newBuilder() {
        GsonBuilder builder = (new GsonBuilder()).registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong());
            }
        }).registerTypeAdapter(List.class, new JsonDeserializer<List>() {
            public List deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
                List list = new ArrayList();
                JsonArray ja = json.getAsJsonArray();
                Iterator var6 = ja.iterator();

                while (var6.hasNext()) {
                    JsonElement je = (JsonElement) var6.next();
                    list.add(je.getAsJsonObject());
                }

                return list;
            }
        }).registerTypeAdapter(Object[].class, new JsonDeserializer<Object[]>() {
            public Object[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                List list = new ArrayList();
                JsonArray ja = json.getAsJsonArray();
                Iterator var6 = ja.iterator();

                while (var6.hasNext()) {
                    JsonElement je = (JsonElement) var6.next();
                    list.add(je.getAsJsonArray());
                }

                return list.toArray();
            }
        }).registerTypeAdapter(byte[].class, new JsonDeserializer<byte[]>() {
            public byte[] deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
                return Base64.decodeBase64(json.getAsString());
            }
        }).registerTypeAdapter(byte[].class, new JsonSerializer<byte[]>() {
            public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(Base64.encodeBase64String(src));
            }
        });
        return builder;
    }

    public static synchronized Object getObject(String json, Type type) {
        Object obj = newBuilder().create().fromJson(json, type);
        return obj;
    }

    public static synchronized String getJsonTree(Object obj, Type type) {
        JsonElement je = newBuilder().create().toJsonTree(obj, type);
        return je.toString();
    }
}