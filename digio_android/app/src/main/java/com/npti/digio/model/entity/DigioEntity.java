package com.npti.digio.model.entity;

import java.io.Serializable;
import java.util.List;

public class DigioEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<SpotlightEntity> spotlight;

    private List<ProductEntity> products;

    public DigioEntity() {

    }

    public List<SpotlightEntity> getSpotlight() {
        return spotlight;
    }

    public void setSpotlight(List<SpotlightEntity> spotlight) {
        this.spotlight = spotlight;
    }

    public List<ProductEntity> getProducts() {
        return products;
    }

    public void setProducts(List<ProductEntity> products) {
        this.products = products;
    }

}