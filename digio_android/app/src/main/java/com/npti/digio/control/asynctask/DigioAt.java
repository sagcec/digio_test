package com.npti.digio.control.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.npti.digio.control.util.GsonUtils;
import com.npti.digio.model.entity.DigioEntity;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

public class DigioAt extends AsyncTask<String, Integer, Boolean> {

    /**
     *
     */

    private static final String URL = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products";

    /**
     *
     */

    private Context context;

    private HttpURLConnection conn;

    private DigioEntity dto;

    /**
     *
     */

    public DigioAt(Context context)
    {
        this.context = context;
    }

    /**
     *
     */

    public DigioEntity getDto() {
        return dto;
    }

    /**
     *
     */

    @Override
    protected Boolean doInBackground(String... strings) {
        try {
            URL url = new URL(URL);

            this.conn = (HttpsURLConnection) url.openConnection();
            this.conn.setRequestMethod("GET");
            this.conn.setDoInput(true);
            this.conn.connect();

            int responseCode = this.conn.getResponseCode();

            if (responseCode != HttpsURLConnection.HTTP_OK) {
                throw new IOException("HTTP error code: " + responseCode);
            }

            String s = IOUtils.toString(this.conn.getInputStream(), StandardCharsets.UTF_8);

            Gson gson = new GsonBuilder().create();
            this.dto = gson.fromJson(s, DigioEntity.class);

            return true;
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (SocketTimeoutException ste) {
            ste.printStackTrace();
        } catch (UnknownHostException uhe) {
            uhe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (this.conn != null) {
                this.conn.disconnect();
            }
        }

        return false;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        try {
            if (result) {
                // Toast.makeText(this.context, "SUCCESS", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this.context, "FAIL", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.e(":: [SS - ERROR - " + this.getClass().getSimpleName() + "]", e.getMessage());

            Toast.makeText(this.context, "FAIL", Toast.LENGTH_SHORT).show();
        } finally {

        }
    }

}