//
//  SpotlightEntity.swift
//  digio
//
//  Created by Gutemberg Albuquerque Da Silva on 19/10/21.
//

import Foundation

class SpotlightEntity: NSObject, Codable {
    
    /**
     * "name": "XBOX",
     "imageURL": "https://s3-sa-east-1.amazonaws.com/digio-exame/xbox_icon.png",
     "description": "Com o e-Gift Card Xbox vocÃª adquire crÃ©ditos para comprar games, mÃºsica, filmes, programas de TV e muito mais!"
     */
    
    var name: String?
    
    var bannerURL: String?
    
    var desc: String?
    
    /**
     *
     */
    
    override init() {
        
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case bannerURL
        case desc = "description"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        bannerURL = try container.decode(String.self, forKey: .bannerURL)
        desc = try container.decode(String.self, forKey: .desc)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(bannerURL, forKey: .bannerURL)
        try container.encode(desc, forKey: .desc)
    }
    
}
