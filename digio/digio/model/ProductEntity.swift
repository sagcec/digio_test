//
//  Product.swift
//  digio
//
//  Created by Gutemberg Albuquerque Da Silva on 19/10/21.
//

import Foundation

class ProductEntity: NSObject, Codable {
    
    /**
     * "name": "XBOX",
     "imageURL": "https://s3-sa-east-1.amazonaws.com/digio-exame/xbox_icon.png",
     "description": "Com o e-Gift Card Xbox vocÃª adquire crÃ©ditos para comprar games, mÃºsica, filmes, programas de TV e muito mais!"
     */
    
    var name: String?
    
    var imageURL: String?
    
    var desc: String?
    
    /**
     *
     */
    
    override init() {
        
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case imageURL
        case desc = "description"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        imageURL = try container.decode(String.self, forKey: .imageURL)
        desc = try container.decode(String.self, forKey: .desc)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(imageURL, forKey: .imageURL)
        try container.encode(desc, forKey: .desc)
    }
    
}
