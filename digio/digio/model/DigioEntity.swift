//
//  DigioEntity.swift
//  digio
//
//  Created by Gutemberg Albuquerque Da Silva on 19/10/21.
//

import Foundation

class DigioEntity: NSObject, Codable {
    
    /**
     * "name": "XBOX",
     "imageURL": "https://s3-sa-east-1.amazonaws.com/digio-exame/xbox_icon.png",
     "description": "Com o e-Gift Card Xbox vocÃª adquire crÃ©ditos para comprar games, mÃºsica, filmes, programas de TV e muito mais!"
     */
    
    var name: String?
    
    var spotlight: [SpotlightEntity]?
    
    var products: [ProductEntity]?
    
    /**
     *
     */
    
    override init() {
        
    }
    
}
