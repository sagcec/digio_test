//
//  UIImageView.swift
//  digio
//
//  Created by Gutemberg Albuquerque Da Silva on 19/10/21.
//

import Foundation
import UIKit

extension UIImageView {
    
    func load(url: URL) {
        DispatchQueue.global().async {
            [weak self] in
            
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                } else {
                    DispatchQueue.main.async {
                        self?.image = UIImage(named: "bin.xmark")
                    }
                }
            }
            
        }
    }
    
    func load(url: URL, completion: @escaping (_ error: Bool) -> Void) {
        DispatchQueue.global().async {
            [weak self] in
            
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                        
                        completion(false)
                    }
                } else {
                    DispatchQueue.main.async {
                        self?.image = UIImage(named: "bin.xmark")
                        
                        completion(true)
                    }
                }
            }
            
        }
    }
    
}
