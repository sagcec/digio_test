//
//  ProductMur.swift
//  digio
//
//  Created by Gutemberg Albuquerque Da Silva on 19/10/21.
//

import Foundation

class DigioMur {
    
    /**
     * DEFAULT
     */
    
    let URL_PATH = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products"
    
    let url: URL!
    
    let nmuRequest: NSMutableURLRequest!
    
    /**
     *
     */
    
    /**
     * CONSTRUCT
     */
    
    init() {
        self.url = URL(string: self.URL_PATH)
        
        self.nmuRequest = NSMutableURLRequest(url: self.url!)
    }
    
    /**
     *
     */
    
    func doBackground(completion: @escaping (_ ok: Bool, _ ent: DigioEntity?) -> Void) {
        self.nmuRequest.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: self.nmuRequest as URLRequest) {
            data, response, error in
            guard
                let data = data,   // unwrap data
                error == nil,      // se nao houver error
                (response as? HTTPURLResponse)?.statusCode == 200   // e status code for igual a 200
            else {
                DispatchQueue.main.async {
                    completion(false, nil)
                }
                
                return
            }
            
            do {
                let jd = JSONDecoder()
                let obj = try jd.decode(DigioEntity.self, from: data)
                
                print("")
                
                DispatchQueue.main.async {
                    completion(true, obj)
                }
            } catch let error {
                debugPrint(error)
                
                DispatchQueue.main.async {
                    completion(false, nil)
                }
            }
            
        }.resume()
    }
    
}
