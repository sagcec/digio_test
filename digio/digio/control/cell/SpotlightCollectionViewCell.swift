//
//  SpotlightCollectionViewCell.swift
//  digio
//
//  Created by Gutemberg Albuquerque Da Silva on 19/10/21.
//

import UIKit

class SpotlightCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var aivLoading: UIActivityIndicatorView!
    
    @IBOutlet weak var ivBanner: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
