//
//  ViewController.swift
//  digio
//
//  Created by Gutemberg Albuquerque Da Silva on 19/10/21.
//

import UIKit
import KVLoading

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    /**
     *
     */
    
    @IBOutlet weak var cvSpotlightList: UICollectionView!
    
    @IBOutlet weak var cvProductList: UICollectionView!
    
    @IBOutlet weak var aivFirst: UIActivityIndicatorView!
    
    @IBOutlet weak var aivSecond: UIActivityIndicatorView!
    
    /**
     *
     */
    
    var de: DigioEntity?
    
    /**
     *
     */

    override func loadView() {
        super.loadView()
        
        self.load()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.fill()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    deinit {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /**
     *
     */
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.cvSpotlightList:
            if let count = self.de?.spotlight?.count {
                return count
            }
            
            return 0
        case self.cvProductList:
            if let count = self.de?.products?.count {
                return count
            }
            
            return 0
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // var cell: UICollectionViewCell!
        
        switch collectionView {
        case self.cvSpotlightList:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! SpotlightCollectionViewCell
            //cell.ivBanner = UIImage()
            
            let url = URL(string: (self.de?.spotlight?[indexPath.row].bannerURL)!)
            
            cell.ivBanner.load(url: url!) {
                error in
                
                cell.aivLoading.isHidden = true
                
            }
            
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! ProductCollectionViewCell
            
            let url = URL(string: (self.de?.products?[indexPath.row].imageURL)!)
            
            cell.ivImage.load(url: url!) {
                error in
                
                cell.aivLoading.isHidden = true
                
                if (error) {
                    print()
                }
                
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        switch collectionView {
        case self.cvSpotlightList:
            let cell = collectionView.cellForItem(at: indexPath) as! SpotlightCollectionViewCell
            
            vc.spotlight = self.de?.spotlight?[indexPath.row]
            
            print("sp")
        default:
            let cell = collectionView.cellForItem(at: indexPath) as! ProductCollectionViewCell
            
            vc.product = self.de?.products?[indexPath.row]
            
            print("pr")
        }
        
        self.present(vc, animated: true)
    }
    
    /**
     *
     */
    
    private func load() {
        self.cvSpotlightList.delegate = self
        self.cvSpotlightList.dataSource = self
        
        self.cvProductList.delegate = self
        self.cvProductList.dataSource = self
        
        self.cvProductList.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cellId")
        self.cvSpotlightList.register(UINib(nibName: "SpotlightCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cellId")
    }
    
    private func start() {
        self.de = DigioEntity()
        
        KVLoading.shared.show(animated: true)
    }
    
    private func fill() {
        let mur = DigioMur()
        mur.doBackground { ok, ent in
            //KVLoading.shared.hide()
            
            guard ok else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.aivFirst.isHidden = true
                    self.aivSecond.isHidden = true
                })
                
                return
            }
            
            self.de = ent
            
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                self.aivFirst.isHidden = true
                self.aivSecond.isHidden = true
                
                self.cvSpotlightList.reloadData()
                self.cvProductList.reloadData()
            })
        }
    }

}
