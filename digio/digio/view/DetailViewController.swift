//
//  DetailViewController.swift
//  digio
//
//  Created by Gutemberg Albuquerque Da Silva on 19/10/21.
//

import UIKit

class DetailViewController: UIViewController {
    
    /**
     *
     */
    
    @IBOutlet weak var lTitle: UILabel!
    
    @IBOutlet weak var ivImage: UIImageView!
    
    @IBOutlet weak var lDescription: UILabel!
    
    /**
     *
     */
    
    var product: ProductEntity?
    
    var spotlight: SpotlightEntity?
    
    /**
     *
     */
    
    override func loadView() {
        super.loadView()
        
        self.load()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.fill()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    deinit {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /**
     *
     */
    
    private func load() {
        
    }
    
    private func start() {
        
    }
    
    private func fill() {
        if let obj = self.spotlight {
            self.lTitle.text = obj.name
            self.lDescription.text = obj.desc
            
            if let sl = self.spotlight {
                self.ivImage.load(url: URL(string: sl.bannerURL!)!) {
                    error in
                    
                    print()
                    
                }
            }
        } else if let obj = self.product {
            self.lTitle.text = obj.name
            self.lDescription.text = obj.desc
            
            if let sl = self.product {
                self.ivImage.load(url: URL(string: sl.imageURL!)!) {
                    error in
                    
                    print()
                    
                }
            }
        }
    }

}
